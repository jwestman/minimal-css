compile:
	sass minimal.sass minimal.css --style compressed

watch:
	sass --watch minimal.sass:minimal.css
