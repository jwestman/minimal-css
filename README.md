# minimal.css
A simple stylesheet for simple webpages

The modern Web is full of eye-popping, heavy, javascript-laden sites. But some
things just don't have to be complicated. HTML is a powerful tool by itself,
and it's more than capable of producing an informative, complete website.

But it could use a little help. minimal.css is a <1kb stylesheet that makes
plain HTML just a little nicer.

## Features
- Responsive column width so your site looks good on huge and tiny screens, and
  everything in between
- Images are prevented from overflowing the width of the column
- Automatic support for dark mode using the prefers-color-scheme: dark media
  query

Plus a couple other changes. For example, blockquotes have a border on the left
so they stand out more, and the font is changed to sans-serif. In general,
minimal.css tries to change as little as possible from the defaults.

## Using minimal.css
The best option is to [download minimal.css](https://minimalcss.jwestman.net/minimal.min.css)
and copy it into your site files. This gives you full control of how the file
is downloaded; no third party sites are involved.

Another easy option is to link it directly in your HTML using the following
code in your `head` tag:

    <link href="https://minimalcss.jwestman.net/minimal.min.css" rel="stylesheet" referrerpolicy="no-referrer" />

Either way, you should also add the following to your `<head>` tag to make sure
your site is the correct size on mobile:

    <meta name="viewport" content="width=device-width, initial-scale=1" />

## Demo
[This demo page](https://minimalcss.jwestman.net/test.html) demonstrates
how many HTML tags appear using minimal.css.

## Development
[View Repository on GitLab](https://gitlab.com/jwestman/minimal-css)

I consider minimal.css to be basically complete so please file an issue before
a merge request, no matter how minor the change is.

If you want to compile minimal.css for yourself, just clone the repository and
edit minimal.sass. Then use the provided Makefile to compile your changes:

- `make` to compile once
- `make watch` to monitor changes and recompile automatically

## Copyright
![Public Domain](https://licensebuttons.net/p/zero/1.0/88x31.png)

minimal.css is public domain under the CC0 public domain dedication. You may
have received a copy of it with the code; if not,
[see here](https://creativecommons.org/publicdomain/zero/1.0/).
